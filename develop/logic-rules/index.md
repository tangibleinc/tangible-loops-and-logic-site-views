
# Logic rules

To add conditional rules that integrate with a loop type, register them with the Tangible Logic module.

The goal is to allow building conditions in an intuitive way, close to natural English.

In practice, the conditional UI is typically used on the admin side to control visibility of templates or builder widgets, based on user-constructed rule groups.

#### Reference

- [Core conditional rules](https://bitbucket.org/tangibleinc/tangible-template-module/src/master/logic/) defined in the Template module
- [Documentation for the Logic module](https://docs.tangible.one/modules/logic/)
  - [Rule definition schema](https://docs.tangible.one/modules/logic/#conditions-config)
  - [Evaluator](https://docs.tangible.one/modules/logic/#evaluate-rule-groups)

## Register

First, get an instance of the Logic module.

```php
$logic = tangible_logic();
```

This can be done within or after the `plugins_loaded` action.

Then call the `extend_rules_by_category` method.

```php
$logic->extend_rules_by_category(
  'woocommerce',
  $rules,
  $evaluator
);
```

It accepts the following arguments:

- **Category name** (string)

  Used to group rule definitions and generate documentation

- **Rule definitions** (array)

  This is the same as the `fields` property in the [conditions config](https://docs.tangible.one/modules/logic/#conditions-config) for the Logic module.

  Each rule definition is an associative array with:
  
  - name
  - label
  - operands
  - values

- **Evaluator** (anonymous function, or function name as string)

  This is the same as the [evaluator](https://docs.tangible.one/modules/logic/#evaluate-rule-groups) for the Logic module.

  It is a function that receives a rule to evaluate, and returns true or false (boolean).
