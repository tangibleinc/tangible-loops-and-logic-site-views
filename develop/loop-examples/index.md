# Loop usage examples

Create a loop from type name

```php
$loop = tangible_loop();
$post_loop = $loop->create_type('post');
```

Check if loop is empty

```php
if ( ! $post_loop->has_next() ) return;
```

Paginated items - all by default

```php
$ids = $post_loop->get_items();
```

All items regardless of pagination

```php
$all_ids = $post_loop->get_all_items();
```

Forward cursor to next item

```php
$item = $post_loop->next();
```

Get field value

```php
$value = $post_loop->get_field('title');
```

Classic WordPress post loop

```php
while( $post_loop->has_next() ) {

  $item = $post_loop->next();
}

// Reset cursor
$post_loop->reset();
```

### Loop operations

Each item

```php
$post_loop->each(function($id) {

  // $this is the loop instance

  $item = $this->get_current();
  $value = $this->get_field('field_name');
});
```

Map items into an array

```php
$results = $post_loop->map(function($id) {
  return [
    'id' => $id,
    'title' => $this->get_field('title')
  ];
});
```

Reduce items to a single value, such as an associative array

```php
$init_state = [
  'ids' => []
  'count' => 0
];

$final_state = $post_loop->reduce(function($state, $id) {
  $state['ids'] []= $id;
  $state['total']++;
  return $state;
}, $init_state);// Pass initital state (optional, defaut is empty array)
```
