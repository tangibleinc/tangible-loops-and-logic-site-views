<Markdown>
# Date

The `Date` tag displays a date.

For example:

```html
<Date />
```

Result: <Date />

It shows today's date by default.

<a name="convert"></a>
## Convert

The `Date` tag can accept a given date in a fairly flexible format.

```html
<Date>now</Date>
<Date>today</Date>
<Date>yesterday</Date>
<Date>2012-12-22</Date>
<Date>-1 week</Date>
<Date>+3 months</Date>
<Date>+5 years</Date>
```

<a name="convert-from-format"></a>
### From format

The most reliable date formats to use are timestamps, `Y-m-d`, and `Y-m-d H:i:s`.

Some formats present difficulty for comparison, such as `d/m/Y` or `m.d.Y` (which are unclear if day or month comes first), and `Ymd` (which cannot be distinguished from timestamp).

Use the `from_format` attribute to convert such values to a standard format.

**Example**

```html
<Date from_format="d/m/Y">2/3/2022</Date>
<Date from_format="m.d.Y">2.3.2022</Date>
```

The `Field` tag also has the `from_format` attribute.

```html
<Field some_field date_format=timestamp from_format="d/m/Y" />
```

<a name="format"></a>
## Format

By default, the `Date` tag uses the date format defined in the admin under Settings -&gt; General -&gt; Date Format.

Use the `format` attribute to format the date differently.

```html
<Date format="Y-m-d H:i:s" />
```

Result: <Date format="Y-m-d H:i:s" />

The date format syntax is based on PHP's [DateTime::format](https://www.php.net/manual/en/datetime.format.php).

Note that timezone-related formats `e`, `p`, and `T` are not supported.

### Common formats

- `Y-m-d H:i:s` (<Date format="Y-m-d H:i:s" />)
- `F j, Y, g:i a` (<Date format="F j, Y, g:i a" />)
- `m.d.y` (<Date format="m.d.y" />)
- `d/m/y` (<Date format="d/m/y" />)

### Timestamp

Use the format `timestamp`, or `U`, to get a UNIX timestamp which can be further converted reliably.

```html
<Date format=timestamp />
```

### Ago

Use the format `ago` to express a past date relative to now.

```html
<Date format=ago>2021-01-01</Date>
```

Result: <Date format=ago>2021-01-01</Date>

<!--

#### Duration

Use the format `duration` to express a time duration.

```html
<Date format=duration>2 years</Date>
```

Result: <Date format=duration>2 years</Date>

-->

<a name="locale"></a>
## Locale

Use the `locale` attribute to translate the result.

```html
<Date locale=fr />
```

Result: <Date locale=fr />

#### Language codes

It accepts language codes based on [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).

</Markdown>
<?php

$translator = Tangible\Carbon\Translator::get();
$locales = $translator->getAvailableLocales();

?>
<p>
  Here is the complete list of <?= count($locales) ?> locales supported.
  <pre id="#locale-list"><code><?= implode(', ', $locales) ?> </code></pre>
</p><?php
?>
<Markdown>

### All locale

Use the `all_locale` attribute to set the default locale for all subsequent uses of the `Date` tag.

For example:

```html
<Date all_locale=fr />
```

After this, the Date tag will translate everything to French.

```html
<Date format="l j F Y" />
```

<Date all_locale=fr />

Result: <Date format="l j F Y" />

To get the current default locale:

```html
<Date all_locale />
```

Result: <Date all_locale />

<Note> Restore </Note>
<Date all_locale=en />


<a name="timezone"></a>
## Time zone

By default, the `Date` tag uses the time zone setting in the admin under Settings -&gt; General -&gt; Timezone.

Use the `timezone` attribute to convert to a different time zone.

```html
<Date format="Y-m-d H:i:s" timezone="America/New_York" />
```

Result: <Date format="Y-m-d H:i:s" timezone="America/New_York" />

The time zone syntax is based on PHP's DateTimeZone class. Here is [the list of supported time zones](https://www.php.net/manual/en/timezones.php).

#### Common time zones

##### The United States

| Common name | Time zone |
|---|---|
| Eastern | `America/New_York` |
| Central | `America/Chicago` |
| Mountain | `America/Denver` |
| Mountain no DST | `America/Phoenix` |
| Pacific | `America/Los_Angeles` |
| Alaska | `America/Anchorage` |
| Hawaii | `America/Adak` |
| Hawaii no DST | `Pacific/Honolulu` |

##### Canada

| Common name | Time zone |
|---|---|
| Newfoundland | `America/St_Johns` |
| Atlantic | `America/Halifax` |
| Atlantic no DST | `America/Blanc-Sablon` |
| Eastern | `America/Toronto` |
| Eastern no DST | `America/Atikokan` |
| Central | `America/Winnipeg` |
| Central no DST | `America/Regina` |
| Mountain | `America/Edmonton` |
| Mountain no DST | `America/Creston` |
| Pacific | `America/Vancouver` |


### Time zone setting

To get the time zone set in the admin:

```html
<Setting timezone_string />
```


<a name="add-subtract"></a>
## Add/subtract

Use the `add` or `subtract` attribute to adjust a given date.

```html
<Date add="1 week">2020-01-01</Date>
<Date subtract="1 week">2020-01-01</Date>
```

Result:

<Date add="1 week">2020-01-01</Date><br/><Date subtract="1 week">2020-01-01</Date>


<a name="field-value"></a>
## Field value

Use the `Field` tag to pass a field value to the `Date` tag.

```html
<Date><Field publish_date /></Date>
```

The `Field` tag has the attribute `date_format` as a shortcut.

```html
<Field publish_date date_format="F j, Y" />
```

### From format

Use the `from_format` attribute to convert from a non-standard format.

```html
<Field some_field date_format="timestamp" from_format="d/m/Y" />
```

See [Convert: From Format](#convert-from-format) for an explanation.

</Markdown>