<Note>

Create a list of defined fields

Set map=fields before loading

</Note>

<a name="fields"></a>
<h2>Fields</h2>

<ul>
  <Loop map_keys=fields>
    <li>
      <code class="color-blue" render><Field key /></code>

      <Loop field=value>
        - <Field description /><br>
      </Loop>
    </li>
  </Loop>
</ul>
