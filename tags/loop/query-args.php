<Note>

Create a list of defined query arguments

Set map=query_args before loading

</Note>

<a name="query"></a>
<h2>Query parameters</h2>

<ul>
  <Loop map_keys=query_args>
    <li>
      <code class="color-blue" render><Field key /></code>

      <Loop field=value>
        - <Field description /><br/>
          <If field=type>
            Type: <Field type format=join glue=", " /><br>
          </If>
          <If field=default>
            Default: <code class="color-value" render><Field default /></code><br>
          </If>
      </Loop>
    </li>
  </Loop>
</ul>
