<?php

$loop_configs = $loop->get_type_configs();

$config = $loop_configs[ $loop_type ];
$query_args = $config['query_args'];

foreach ($query_args as $key => $value) {
  if (isset($value['internal'])) unset( $query_args[ $key ] );
}

$html->set_map('query_args', $query_args);

$fields = $config['fields'];

foreach ($fields as $key => $value) {
  if (isset($value['internal'])) unset( $fields[ $key ] );
}

$html->set_map('fields', $fields);

require_once __DIR__.'/query-args.php';
require_once __DIR__.'/fields.php';
