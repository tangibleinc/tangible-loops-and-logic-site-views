# Tangible Loops & Logic - Site views

This is a child theme installed on [the Loops & Logic documentation site](https://loop.tangible.one).

It contains templates for the site frontend, which is a documentation (and some testing) of plugin features.

It depends on a parent theme called [Tangible Views](https://bitbucket.org/tangibleinc/tangible-views).
