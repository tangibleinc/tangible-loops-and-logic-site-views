
# Advanced Custom Fields

The `Loop` and `Field` tags support the following ACF field types.

#### Basic

- [Text](#basic)
- [Textarea](#basic)
- [Number](#basic)
- [Email](#basic)
- [URL](#basic)
- [Password](#basic)

#### Content

- [WYSIWYG editor](#editor)
- [OEmbed](#oembed)
- [Template](#template)

#### Date

- [Date picker](#date)
- [Date-time picker](#date)
- [Time picker](#date)

#### Choice

- [Select](#select)
- [Checkbox](#checkbox)
- [Radio](#radio)
- [True/False](#true_false)

#### Relational

- [Link](#link)
- [File](#file)
- [Image](#image)
- [Gallery](#gallery)
- [Post object](#post-object)
- [Relationship](#relationship)
- [Taxonomy](#taxonomy)
- [User](#user)

#### Nested

- [Repeater](#repeater)
- [Flexible content](#flexible-content)
- [Group](#group)


#### [Fields from options page](#from-options)

<br/>

# Field types

<a name="basic"></a>
## Basic fields

These field types can be displayed simply with the `Field` tag.

- Text
- Textarea
- Number
- Email
- URL
- Password

## Content fields

<a name="editor"></a>
### WYSIWYG editor

Use the `acf_editor` attribute.

```html
<Field acf_editor=field_name />
```

<a name="oembed"></a>
### OEmbed

Use the `acf_oembed` attribute.

```html
<Field acf_oembed=field_name />
```

<a name="template"></a>
### Template

This is for the ACF field type "Tangible Template" (under Content section) added by the plugin.

Use the `acf_template` or `template` attribute to render the template field.

```html
<Field acf_template=field_name />
```

## Date fields

<a name="date"></a>
### Date, date-time, and time picker

Use the `acf_date`, `acf_date_time`, or `acf_time` attribute.

```html
<Field acf_date=field_name />
<Field acf_date_time=field_name />
```

#### Date format

Optionally, use the `format` and `locale` attributes to apply date formatting.

```html
<Field acf_date=field_name format="l j F Y" locale=fr />
```

For format syntax and available locales, see [Format: Date](/tags/format#date).

Use `format=default` to use the site setting from Settings -&gt; General -&gt; Date Format.


## Choice

<a name="select"></a>
### Select

Use the `acf_select` attribute for a field with single selected value.

```html
<Field acf_select=field_name />
```

For multiple values, use the `Loop` tag.

```html
<Loop acf_select=field_name>
  <Field />
</Loop>
```

<a name="checkbox"></a>
### Checkbox

Use the `Loop` tag and `acf_checkbox` attribute.

```html
<Loop acf_checkbox=field_name>
  <Field />
</Loop>
```

<a name="radio"></a>
### Radio

Use the `acf_radio` attribute.

```html
<Field acf_radio=field_name />
```

<a name="true_false"></a>
### True/False

Use the `acf_true_false` attribute.

```html
<If acf_true_false=field_name>
  TRUE
<Else />
  FALSE
</If>
```

---

<a name="choice-labels"></a>
### Choice labels

Use the `Field` tag and `field=label` to get the label of the chosen value.

```html
<Field acf_select=field_name field=label />
```

Use the `Loop` tag and `field=labels` for fields with multiple values.

```html
<Loop acf_select=field_name field=labels>
  Label: <Field />
</Loop>
```

<a name="choices"></a>
### Choices

Use the `Loop` tag and `field=choices` to loop through the available choices of a field.

```html
<Loop acf_select=field_name field=choices>
  Value: <Field value /><br/>
  Label: <Field label /><br/>
</Loop>
```

It is a list of choices with `value` and `label` fields.

## Relational

For these field types, use the `Loop` tag to get associated content.

Use the `Field` tag as a shortcut to get a subfield. For example:

```html
<Field acf_file=field_name field=title />
```

<a name="file"></a>
### File

Use the `acf_file` attribute.

```html
<Loop acf_file=field_name>
  <Field title />
</Loop>
```

```html
<Field acf_file=field_name field=url />
```

See [Attachment Loop](/tags/loop/attachment#fields) for available fields.

<a name="image"></a>
### Image

Use the `acf_image` attribute.

```html
<Loop acf_image=field_name>
  <Field title />
</Loop>
```

```html
<Field acf_image=field_name field=url />
```

See [Attachment Loop](/tags/loop/attachment#fields) for available fields.

<a name="link"></a>
### Link

Use the `acf_link` attribute.

```html
<Loop acf_link=field_name>
  <Field title />
</Loop>
```

```html
<Field acf_link=field_name field=url />
```

Available fields are: `url`, `title`, and `target`.


<a name="post-object"></a>
### Post object

Use the `acf_post` attribute.

```html
<Loop acf_post=field_name>
  <Field title />
</Loop>
```

See [Post Loop](/tags/loop/post#fields) for available fields.

<a name="relationship"></a>
### Relationship

Use the `acf_relationship` attribute.

```html
<Loop acf_relationship=field_name>
  <Field title />
</Loop>
```

See [Post Loop](/tags/loop/post#fields) for available fields.

<a name="taxonomy"></a>
### Taxonomy

Use the `acf_taxonomy` attribute.

```html
<Loop acf_taxonomy=field_name>
  <Field title />
</Loop>
```

See [Taxonomy Term Loop](/tags/loop/taxonomy-term#fields) for available fields.


<a name="user"></a>
### User

Use the `acf_user` attribute.

```html
<Loop acf_user=field_name>
  <Field full_name />
</Loop>
```

See [User Loop](/tags/loop/user#fields) for available fields.


<a name="gallery"></a>
### Gallery

Use the `acf_gallery` attribute.

```html
<Loop acf_gallery=field_name>
  <Field title />
</Loop>
```

See [Attachment Loop](/tags/loop/attachment#fields) for available fields.

## Nested

<a name="repeater"></a>
### Repeater

Use the `acf_repeater` attribute.

```html
<Loop acf_repeater=field_name>
  <Field title />
</Loop>
```

All field types are supported inside a repeater loop, including another repeater field.

<a name="flexible-content"></a>
### Flexible content

Use the `acf_flexible` attribute.

```html
<Loop acf_flexible=field_name>
  <Field title />
</Loop>
```

All field types are supported inside a flexible content loop, including another flexible content field.

#### Layout

Use the field `layout` to check which layout was chosen for each item.

```html
<If field=layout value=layout_1>
  Layout 1
<Else if field=layout value=layout_2 />
  Layout 2
</If>
```


<a name="group"></a>
### Group

Use the `acf_group` attribute.

```html
<Loop acf_group=field_name>
  <Field title />
</Loop>
```


<a name='from-options'></a>
## Fields from options page

ACF provides a function called [acf_add_options_page](https://www.advancedcustomfields.com/resources/acf_add_options_page/) to add options pages to store global settings.

To get fields from an options page, use the Field or Loop tag with the attribute `from=options`.


